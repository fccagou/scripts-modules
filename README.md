# Presentation

This repo is a puppet like modules management for common scripts functions.

The goal is to make together all the code we usually copy/paste in each scripts.
It's firt made for shell scripts but it can be enhanced to add other langages
libs or tools.


# Quick start

First makes a modules dir into your project then clone this project :

    mkdir modules
    git clone https://gitlab.com/fccagou/scripts-modules.git modules/
    git clone https://gitlab.com/scripts-modules/log.git modules/log


In main program, init vars and load modules

    # --------------------------------------------------------------------
    #  Loading common tools
    # --------------------------------------------------------------------
    FCCAGOU_SHELL_MODULES_INIT="${FCCAGOU_SHELL_MODULES_INIT:-"$(dirname "$0")"/modules/scripts-modules/init.sh}"
    
    [ -r "${FCCAGOU_SHELL_MODULES_INIT}" ] || {
        printf -- "[-] %s\n"  "Can't open ${FCCAGOU_SHELL_MODULES_INIT}... exiting"
        exit 1
    }
    
    readonly FCCAGOU_SHELL_MODULES_INIT
    export FCCAGOU_SHELL_MODULES_INIT

    # For local modules, create site-modules dir and set FCCAGOU_SHELL_MODULES_PATH 
    # FCCAGOU_SHELL_MODULES_PATH="$(dirname "$0")/site-modules"
    
    # shellcheck source=./libs/init.sh
    source "$FCCAGOU_SHELL_MODULES_INIT"
    # --------------------------------------------------------------------


then include module with `require` function.

    require 'log'
    require 'net'
    ...


# Add you own modules

To create new module

    mkdir modules/mymodule
    touch modules/mymodule/init.sh
    # then code :P

You can also clone remote module :

   cd modules && git clone your/remote/module-xxx.git .

To make easier the way to find a function, function names must be in the form `modulename_xxxxxxx`.


# Todo

This is the beginœing of the project, many ideas come writing it.
* use a modulefile
* write a best practices doc
* add test case for CI

All we want to share there.


