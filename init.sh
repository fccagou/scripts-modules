#!/usr/bin/env bash


action () {
    printf -- "[+] %s\n"  "${@}"
}

info () {
    printf -- "[*] %s\n"  "${@}"
}

error () {
    printf -- "[-] %s\n"  "${@}"
}

warning () {
    printf -- "[!] %s\n"  "${@}"
}

exit_on_error () {
    error "$@"
    exit 1
}

# ----------------------------------
# MODULES
# ----------------------------------

require () {
    local modulename
    local found
    local modulepath

    found="no"
    modulepath=""
    modulename="$1"

    [ -z "${FCCAGOU_SHELL_MODULES_PATH}" ] && exit_on_error "require $modulename: Variable FCCAGOU_SHELL_MODULES_PATH not set"
    for p in ${FCCAGOU_SHELL_MODULES_PATH//:/ }
    do
        [ -r "$p/$modulename/init.sh" ]  &&  {
            modulepath="$p"
            found="yes"
            break
        }
    done

    [ "found" = "no" ] && exit_on_error "Unable to import module $modulename"
    source "$modulepath/$modulename/init.sh"
}

# ----------------------------------
#   MAIN CHECKS
# ----------------------------------
if [ -z "${FCCAGOU_SHELL_MODULES_INIT}" ]
then
    warning "variable FCCAGOU_SHELL_MODULES_INIT not defined"
else
    if [ -z "${FCCAGOU_SHELL_MODULES_PATH}" ]
    then
        FCCAGOU_SHELL_MODULES_PATH="$(cd "$(dirname "${FCCAGOU_SHELL_MODULES_INIT}")/.." && pwd )"
    else
        FCCAGOU_SHELL_MODULES_PATH="${FCCAGOU_SHELL_MODULES_PATH}:$(cd "$(dirname "${FCCAGOU_SHELL_MODULES_INIT}")/.." && pwd )"
    fi
    readonly FCCAGOU_SHELL_MODULES_PATH
    export FCCAGOU_SHELL_MODULES_PATH
fi

require "log"

